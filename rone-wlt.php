<?php
/*
 * @category            WordPress_Plugin
 * @package             RO_WLT
 * @author              Tim Russell <githubber@timrussell.com>
 * @copyright           Copyright (c) 2014-2015
 * @license             GPL-2.0+
 *
 *
 * @wordpress-plugin
 * Plugin Name:         RONE WLT
 * Plugin URI:          https://gitlab.com/tdavidrussell/rone-wlt
 * Description:         Logs user's screen size and color depth in site web logs, by loading image using javascript. See <a href="http://www.weblogexpert.com/">Weblog Expert</a> log analyzer.
 * Version:             20181202.1
 * Author:              Tim Russell
 * Author URI:          https://www.timrussell.com/
 * License:             GPL-2.0+
 * License URI:         http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:         rone-wlt
 * Domain Path:         /languages
 * XXXGitLab Plugin URI:   https://gitlab.com/tdavidrussell/rone-wlt
 * GitLab Branch:       master
 *
 * Requires WP:         4.8
 * Requires PHP:        5.6
 *
 * Support URI:         https://gitlab.com/tdavidrussell/rone-wlt
 * Documentation URI:   https://gitlab.com/tdavidrussell/rone-wlt
 *
 * Tags: plugin,
  *
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** initial constants **/
define( 'ROWLT_PLUGIN_VERSION', '20181202.1' );
define( 'ROWLT_PLUGIN_DEBUG', false );
//
define( 'ROWLT_PLUGIN_URI', plugin_dir_url( __FILE__ ) ); //Does contain trailing slash
define( 'ROWLT_PLUGIN_PATH', plugin_dir_path( __FILE__ ) ); //Does contain trailing slash
//
define( 'ROWLT_THEME_DIR', get_stylesheet_directory() );    //Does NOT contain a trailing slash
define( 'ROWLT_THEME_URI', get_stylesheet_directory_uri() ); //Does not contain a trailing slash

/**
 * Prep the javascript for loading.
 *
 */
function rone_wlt() {

	wp_enqueue_script( "ronewlt", ROWLT_PLUGIN_URI . "rone-wlt.js" );

}

/**
 * Load the scripts, only on the user side.
 */
if ( ! is_admin() ) {
	add_action( 'init', 'rone_wlt' );
	add_action( 'login_head', 'rone_wlt' );
}
?>