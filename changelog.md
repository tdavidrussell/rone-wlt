##Changelog

##### 2011202.1
* Changed: repo location
* Updated: description

##### 20160110.1
* Updated: PHPDoc and description

##### 20150803.1
* First git local commit

##### 20150728.1
* Changed: formatted *.js files
* Bumped version

##### 20150722.1
* Added to git.
* Bumped version.

##### 20150713.1
 * Updated: Plugin description updated.
 * Added: Plugin initial constants where added.
 * Changed: bumped version
 