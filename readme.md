## RONE WTL Plugin

Logs user's screen size and color depth in the site web logs, by loading image using javascript. See [Weblog Expert](http://www.weblogexpert.com/)  log analyzer.


__Contributors:__ [Tim Russell](https://gitlab.com/tdavidrussell)   
__Requires:__ 4.8   
__Tested up to:__ 5.0   
__License:__ [GPL-2.0+](http://www.gnu.org/licenses/gpl-2.0.html)   


### WTL Plugin For WordPress


## License   
This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

## Installation ##

### Upload ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/rone-wlt) from GitLab.
2. Go to the __Plugins &rarr; Add New__ screen in your WordPress admin panel and click the __Upload__ tab at the top.
3. Upload the zipped archive.
4. Click the __Activate Plugin__ link after installation completes.

### Manual ###

1. Download the [latest release](hhttps://gitlab.com/tdavidrussell/rone-wlt) from GitLab.
2. Unzip the archive.
3. Copy the folder to ' /wp-content/plugins/ '.
4. Go to the __Plugins__ screen in your WordPress admin panel and click the __Activate__ link under RONE WLT.

Read the Codex for more information about [installing plugins manually](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).


## Usage ##


## Changelog

See [CHANGELOG](changelog.md).

